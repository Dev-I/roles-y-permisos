<div class="form-group">
    {{ Form::label('name', 'Nombre de producto') }}
    {{ Form::text('name', null, ['class'=> 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('description', 'Descripcion del producto') }}
    {{ Form::text('description', null, ['class'=> 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::submit('Guardar', ['class'=> 'btn bt-sm btn-primary']) }}
</div>