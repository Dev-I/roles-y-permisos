<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //..........Permisos de usuarios
        Permission::create([
           'name'=> 'Navegar usuarios ',
           'slug' =>'users.index',
           'description' => 'lista y navega todos los usuarios del sistema',

        ]);
        Permission::create([
            'name'=> 'Ver detalle de usuarios',
            'slug' =>'users.show',
            'description' => 'Ve en detalle cada usuaio del sistema',

        ]);
        Permission::create([
            'name'=> 'Edicion de usuarios',
            'slug' =>'users.edit',
            'description' => 'Edita cada usuario del sistema',

        ]);
        Permission::create([
            'name'=> 'Elimina usuarios',
            'slug' =>'users.distroy',
            'description' => 'Elimina cada usuario del sistema',

        ]);

        //..........Roles
        Permission::create([
            'name'=> 'Navegador roles',
            'slug' =>'roles.index',
            'description' => 'lista y navega todos los roles del sistema',

        ]);
        Permission::create([
            'name'=> 'Ver detalle roles',
            'slug' =>'roles.show',
            'description' => 'Ve en detalle cada rol del sistema',

        ]);

        Permission::create([
            'name'=> 'Creacion de roles',
            'slug' =>'roles.create',
            'description' => 'Crea un rol del sistema',

        ]);


        Permission::create([
            'name'=> 'Edicion de roles',
            'slug' =>'roles.edit',
            'description' => 'Edita cada rol del sistema',

        ]);
        Permission::create([
            'name'=> 'Elimina roles',
            'slug' =>'roles.distroy',
            'description' => 'Elimina cada rol del sistema',

        ]);

        //--------Producto.......

        Permission::create([
            'name'=> 'Navegador de produtos',
            'slug' =>'products.index',
            'description' => 'lista y navega todos los productos del sistema',

        ]);
        Permission::create([
            'name'=> 'Ver detalle de producto',
            'slug' =>'products.show',
            'description' => 'Ve en detalle cada rol del sistema',

        ]);

        Permission::create([
            'name'=> 'Creacion de productos',
            'slug' =>'products.create',
            'description' => 'Crea un productos del sistema',

        ]);

        Permission::create([
            'name'=> 'Edicion de priductos',
            'slug' =>'product.edit',
            'description' => 'Edita cada rol del sistema',

        ]);
        Permission::create([
            'name'=> 'Elimina produtos',
            'slug' =>'products.distroy',
            'description' => 'Elimina cada rol del sistema',

        ]);
    }
}
