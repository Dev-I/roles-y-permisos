<?php

use Illuminate\Database\Seeder;
use App\User;
use Caffeinated\Shinobi\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'admin',
            'email' =>'admin',
            'password'=>'admin'

        ]);

        Role::create([
            'name'=> 'Admin',
            'slug'=> 'admin',
            'special'=> 'all-access'
        ]);

        factory(User::class, 20)->create();


    }
}
